import { useCallback, useState } from "react";
import { FormControl, Grid, Link, TextField, Typography } from "@mui/material";

import { Drawer } from "../Drawer";
import styles from './App.module.css';

/**
 * Process the input into two dimensional array sorted by line and column.
 * E.g.: arr[line][column]
 *
 * @param input - string input from text field
 * @return array - two dimensional array sorted by line and column
 */
const processInput = (input) => {
  const isRowColumnValid = (row, column) => row && column && !isNaN(row) && !isNaN(column)
  const result = input
    .split('\n')
    .map(line => line.split(';'))

  let maxRows = 0
  let maxColumns = 0

  result.forEach(([ row, column ]) => {
    if (isRowColumnValid(row, column)) {
      maxRows = Math.max(maxRows, row)
      maxColumns = Math.max(maxColumns, column)
    }
  })

  const array = new Array(maxRows).fill(null).map(() => new Array(maxColumns).fill(null))

  result.forEach(([ row, column, ...otherValues ]) => {
    if (isRowColumnValid(row, column)) {
      array[row - 1][column - 1] = otherValues
    }
  })

  return array
}

const App = () => {
  const [ input, setInput ] = useState('')
  const onElementDrawerChange = useCallback((e) => {
    setInput(e.target.value)
  }, [ setInput ])

  return (
    <div className={styles.app}>
      <header className={styles.defaultMargin}>
        <Typography variant="h3">Klynets Viktor Elements Drawer</Typography>
      </header>
      <main className={styles.main}>
        <FormControl fullWidth className={styles.defaultMargin}>
          <TextField
            label="Elements Drawer"
            onChange={onElementDrawerChange}
            value={input}
            multiline
          />
        </FormControl>
        <Grid container>
          {processInput(input).map((row, rowIndex) => (
            <Grid
              key={`line_${rowIndex}`}
              container
              spacing={1}
              justifyContent="space-between"
              className={styles.defaultMargin}
            >
              {row && row.map((column, colIndex) => (
                <Grid key={`${rowIndex}_${colIndex}_${Math.random()}`} item xs className={styles.gridItem}>
                  {column && (
                    <Drawer
                      label={column[0]}
                      type={column[1]}
                      value={column[2]}
                    />
                  )}
                </Grid>
              ))}
            </Grid>
          ))}
        </Grid>
      </main>
      <footer>
        <Typography variant="h5">My Links:</Typography>
        <Grid container justifyContent="space-between" alignItems="center" className={styles.links}>
          <Link href="mailto:klinyecviktor@gmail.com">Email</Link>
          <Link href="https://linkedin.com/in/victor-klynets/">LinkedIn</Link>
        </Grid>
      </footer>
    </div>
  );
}

export default App;
