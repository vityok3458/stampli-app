import { PureComponent } from "react";
import { FormControl, TextField } from "@mui/material";
import { SelectType } from "./Select";

export class Drawer extends PureComponent {
  componentDidMount() {
    console.info('Drawer COMPONENT did mount ✅')
  }

  componentWillUnmount() {
    console.info('Drawer COMPONENT will unmount 💀️')
  }

  render() {
    const {type, label, value} = this.props

    if (type === 'SELECT') {
      if (!value) {
        return null
      }

      return <SelectType label={label} value={value}/>
    }

    if (type === 'TEXT_INPUT') {
      return (
        <FormControl fullWidth>
          <TextField label={label} placeholder={value}/>
        </FormControl>
      )
    }

    return null
  }
}
