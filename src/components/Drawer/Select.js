import { useCallback, useState } from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import styles from './Select.module.css';

export const SelectType = ({ label, value }) => {
  const values = value.split(',')
  const [ selection, setSelection ] = useState(values[0])
  const handleChange = useCallback((event) => {
    setSelection(event.target.value)
  }, [setSelection])

  return (
    <FormControl fullWidth className={styles.width}>
      <InputLabel style={{ textTransform: 'capitalize' }} id="select-label">{label}</InputLabel>
      <Select
        labelId="select-label"
        value={selection}
        label={label}
        onChange={handleChange}
      >
        {values.map(val => (
          <MenuItem key={val} value={val}>{val}</MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}

