# Klynets Viktor Elements Drawer
## Getting started
To check the drawer install the packages and run the application:
```npm
npm ci
npm start
```

Or you can serve the built version off the app via `serve`, `http-serve` or any other static site serve 
```npm
npm install -g serve
serve -s build
```

## Libraries used
- [React](https://reactjs.org)
- [Material-UI](https://mui.com)

Initial repo created by [create-react-app](https://create-react-app.dev/)
